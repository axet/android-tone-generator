# Tone Generator

Android ToneGenerator class preview app!

  * http://developer.android.com/intl/en/reference/android/media/ToneGenerator.html

# Files

Android Source
  * [https://android.googlesource.com/.../ToneGenerator.java](https://android.googlesource.com/platform/frameworks/base/+/master/media/java/android/media/ToneGenerator.java)

Tones array
  * [DefaultTones.java](app/src/main/java/com/github/axet/tonegenerator/DefaultTones.java)

# Example

```java
   import android.media.ToneGenerator;

   // int tone_id = TONE_* from ToneGenerator.java
   
   int volume = 100;
   int durationMs = 1000;

   ToneGenerator generator = new ToneGenerator(AudioManager.STREAM_MUSIC, volume);
   generator.startTone(tone_id, durationMs);
```

# Manual install

    gradle installDebug

# Screenshots

![shot](/docs/shot.png)
