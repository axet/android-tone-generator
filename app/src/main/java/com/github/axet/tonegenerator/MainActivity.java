package com.github.axet.tonegenerator;

import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.AboutPreferenceCompat;
import com.github.axet.androidlibrary.widgets.SearchView;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    TonesAdapter tones;
    ListView list;
    ToneGenerator generator = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);

    public class TonesAdapter extends ArrayAdapter<Tone> {
        int select = -1;
        String filter;
        List<Tone> all;

        public TonesAdapter(Context context, List<Tone> all) {
            super(context, 0);
            this.all = all;
            search(null);
        }

        void search(String s) {
            Tone sel = null;
            if (select >= 0 && select < getCount())
                sel = getItem(select);
            this.filter = s;
            clear();
            for (Tone t : all) {
                if (filter == null) {
                    add(t);
                } else {
                    if (t.description.toLowerCase().contains(filter.toLowerCase())) {
                        add(t);
                    } else if (t.name.toLowerCase().contains(filter.toLowerCase())) {
                        add(t);
                    }
                }
            }
            if (sel != null) {
                for (int i = 0; i < getCount(); i++) {
                    if (getItem(i).id == sel.id)
                        select = i;
                }
            }
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            Tone tone = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item, parent, false);
            }

            if (select == position) {
                convertView.setBackgroundColor(0xff555555);
            } else {
                convertView.setBackgroundColor(0xffffffff);
            }

            // Lookup view for data population
            TextView number = (TextView) convertView.findViewById(R.id.item_number);
            TextView name = (TextView) convertView.findViewById(R.id.item_name);
            //TextView desc = (TextView) convertView.findViewById(R.id.item_desc);

            number.setText("" + tone.id);
            name.setText(tone.name.trim());
            //desc.setText(tone.description.trim());

            return convertView;
        }

        public void select(int i) {
            select = i;
            notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        list = (ListView) findViewById(R.id.list);
        tones = new TonesAdapter(this, Arrays.asList(DefaultTones.tones));
        list.setAdapter(tones);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                load(position);
            }
        });

        findViewById(R.id.tone_prev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tones.select = tones.select - 1;
                if (tones.select < 0)
                    tones.select = 0;
                load(tones.select);
                play();
            }
        });

        findViewById(R.id.tone_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tones.select = tones.select + 1;
                int max = tones.getCount() - 1;
                if (tones.select >= max)
                    tones.select = max;
                load(tones.select);
                play();
            }
        });

        findViewById(R.id.tone_play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play();
            }
        });

        load(0);
    }

    void play() {
        if (tones.select < 0 || tones.select >= tones.getCount())
            return;
        Tone t = tones.getItem(tones.select);
        generator.startTone(t.id, 1000);
    }

    void load(int i) {
        if (i < 0 || i >= tones.getCount())
            return;
        tones.select(i);
        list.smoothScrollToPosition(i);
        Tone t = tones.getItem(i);
        TextView name = (TextView) findViewById(R.id.tone_name);
        TextView desc = (TextView) findViewById(R.id.tone_desc);
        name.setText(t.name.trim());
        desc.setText(t.description.trim());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                tones.search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCloseListener(new android.support.v7.widget.SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                tones.search(null);
                list.setSelection(tones.select);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_about) {
            AboutPreferenceCompat.showDialog(this, R.raw.about);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
